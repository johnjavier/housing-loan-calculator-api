FROM java:8
EXPOSE 8087
ADD /target/housing-loan-calculator-0.0.1-SNAPSHOT housing-loan-calculator.jar
ENTRYPOINT ["java", "-jar", "housing-loan-calculator.jar"]