package com.johnjavier.housingloancalculator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.johnjavier.housingloancalculator.calculator.AmortizationCalculator;
import com.johnjavier.housingloancalculator.calculator.GenericLoanRequestBuilder;
import com.johnjavier.housingloancalculator.calculator.LoanCalculator;
import com.johnjavier.housingloancalculator.model.AmortizationRequest;
import com.johnjavier.housingloancalculator.model.AmortizationResponse;
import com.johnjavier.housingloancalculator.model.LoanRequest;
import com.johnjavier.housingloancalculator.model.LoanResponse;

@Service
public class HousingLoanCalculatorService {

	@Autowired
	private LoanCalculator loanCalculator;

	@Autowired
	private AmortizationCalculator amortizationCalculator;

	/**
	 * Get mortgage details
	 * 
	 * @param request
	 * @return
	 */
	public LoanResponse getMortgage(LoanRequest request) {

		GenericLoanRequestBuilder loanRequestBuilder = GenericLoanRequestBuilder.Builder.newInstance()
				.setInitialDeposit(request.getInitialDeposit()).setInterestRate(request.getInterestRate())
				.setMonthlyExpenses(request.getMonthlyExpenses()).setMonthlyIncome(request.getMonthlyIncome())
				.setTerm(request.getTerm()).build();

		loanCalculator.setGenericLoanRequestBuilder(loanRequestBuilder);

		LoanResponse response = new LoanResponse();
		response.setMaxMonthlyMortgage(loanCalculator.calculateMaxMonthlyMortgage());
		response.setLoanableMortgage(loanCalculator.calculateLoanableMortgage());

		return response;
	}

	/**
	 * Get Amortization details
	 * 
	 * @param request
	 * @return
	 */
	public AmortizationResponse getAmortizationDetails(AmortizationRequest request) {

		GenericLoanRequestBuilder loanRequestBuilder = GenericLoanRequestBuilder.Builder.newInstance()
				.setInitialDeposit(request.getInitialDeposit()).setInterestRate(request.getInterestRate())
				.setTerm(request.getTerms()).setPrincipal(request.getPrincipalLoanAmount()).build();

		return getAmortizationResponse(request, loanRequestBuilder);
	}

	/**
	 * Build amortization response
	 * 
	 * @param request
	 * @param calculator
	 */
	private AmortizationResponse getAmortizationResponse(AmortizationRequest request,
			GenericLoanRequestBuilder loanRequestBuilder) {

		amortizationCalculator.setGenericLoanRequestBuilder(loanRequestBuilder);

		AmortizationResponse amortization = new AmortizationResponse();
		amortization.setAmortization(amortizationCalculator.calculateAmortizationAmount());
		amortization.setInitialDeposit(request.getInitialDeposit());
		amortization.setInterestRate(request.getInterestRate());
		amortization.setPrincipalLoanAmount(request.getPrincipalLoanAmount());
		amortization.setTerms(request.getTerms());

		return amortization;
	}
}
