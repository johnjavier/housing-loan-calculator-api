package com.johnjavier.housingloancalculator.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.johnjavier.housingloancalculator.model.AmortizationRequest;
import com.johnjavier.housingloancalculator.model.AmortizationResponse;
import com.johnjavier.housingloancalculator.model.LoanRequest;
import com.johnjavier.housingloancalculator.model.LoanResponse;
import com.johnjavier.housingloancalculator.service.HousingLoanCalculatorService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "Housing Loan Calculator", description = "Operations to calculate related information about housing loan")
public class HousingLoanCalculatorController {

	@Autowired
	private HousingLoanCalculatorService housingLoanCalculatorService;

	@ApiOperation(value = "Get loanable amount details", response = ResponseEntity.class)
	@GetMapping(value = "/loan", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LoanResponse> getMortgage(@Valid LoanRequest request) {

		return new ResponseEntity<LoanResponse>(housingLoanCalculatorService.getMortgage(request), HttpStatus.OK);
	}

	@ApiOperation(value = "Get amortization amount details", response = ResponseEntity.class)
	@GetMapping(value = "/amortization", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AmortizationResponse> getAmortization(@Valid AmortizationRequest request) {

		return new ResponseEntity<AmortizationResponse>(housingLoanCalculatorService.getAmortizationDetails(request),
				HttpStatus.OK);
	}
}
