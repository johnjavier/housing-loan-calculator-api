package com.johnjavier.housingloancalculator.model;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

public class AmortizationRequest {

	@Positive(message = "Invalid principal amount")
	private double principalLoanAmount;

	@Positive(message = "Invalid loan interest rate")
	private double interestRate;

	@PositiveOrZero(message = "Invalid loan terms")
	private int terms;

	@PositiveOrZero(message = "Invalid initial deposit")
	private double initialDeposit;

	public double getPrincipalLoanAmount() {
		return principalLoanAmount;
	}

	public void setPrincipalLoanAmount(double principalLoanAmount) {
		this.principalLoanAmount = principalLoanAmount;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public int getTerms() {
		return terms;
	}

	public void setTerms(int terms) {
		this.terms = terms;
	}

	public double getInitialDeposit() {
		return initialDeposit;
	}

	public void setInitialDeposit(double initialDeposit) {
		this.initialDeposit = initialDeposit;
	}

}
