package com.johnjavier.housingloancalculator.model;

public class LoanResponse {

	public double maxMonthlyMortgage;

	public double loanableMortgage;

	public double getMaxMonthlyMortgage() {
		return maxMonthlyMortgage;
	}

	public void setMaxMonthlyMortgage(double monthlyMortgage) {
		this.maxMonthlyMortgage = monthlyMortgage;
	}

	public double getLoanableMortgage() {
		return loanableMortgage;
	}

	public void setLoanableMortgage(double loanableMortgage) {
		this.loanableMortgage = loanableMortgage;
	}

}
