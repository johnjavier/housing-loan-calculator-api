package com.johnjavier.housingloancalculator.model;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "Class representing a loan request to compute the maximum loanable amount")
public class LoanRequest {

	@PositiveOrZero(message = "Invalid initial deposit")
	private double initialDeposit;

	@Positive(message = "Invalid monthly income. Should be greater than 0")
	private double monthlyIncome;

	@Positive(message = "Loan interest rate cannot be blank")
	private double interestRate;

	@Positive(message = "Invalid loan term")
	private int term;

	@PositiveOrZero(message = "Invalid monthly expenses.")
	private double monthlyExpenses;

	public double getInitialDeposit() {
		return initialDeposit;
	}

	public void setInitialDeposit(double initialDeposit) {
		this.initialDeposit = initialDeposit;
	}

	public double getMonthlyIncome() {
		return monthlyIncome;
	}

	public void setMonthlyIncome(double monthlyIncome) {
		this.monthlyIncome = monthlyIncome;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public int getTerm() {
		return term;
	}

	public void setTerm(int term) {
		this.term = term;
	}

	public double getMonthlyExpenses() {
		return monthlyExpenses;
	}

	public void setMonthlyExpenses(double monthlyExpenses) {
		this.monthlyExpenses = monthlyExpenses;
	}

}
