package com.johnjavier.housingloancalculator.model;

public class AmortizationResponse {

	private double principalLoanAmount;

	private double interestRate;

	private int terms;

	private double initialDeposit;

	private double amortization;

	public double getPrincipalLoanAmount() {
		return principalLoanAmount;
	}

	public void setPrincipalLoanAmount(double principalLoanAmount) {
		this.principalLoanAmount = principalLoanAmount;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public int getTerms() {
		return terms;
	}

	public void setTerms(int terms) {
		this.terms = terms;
	}

	public double getInitialDeposit() {
		return initialDeposit;
	}

	public void setInitialDeposit(double initialDeposit) {
		this.initialDeposit = initialDeposit;
	}

	public double getAmortization() {
		return amortization;
	}

	public void setAmortization(double amortization) {
		this.amortization = amortization;
	}

}
