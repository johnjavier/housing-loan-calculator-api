package com.johnjavier.housingloancalculator.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("housingloancalculator.mortgage")
public class HousingLoanCalculatorConfiguration {

	private double risk;

	public double getRisk() {
		return risk;
	}

	public void setRisk(double risk) {
		this.risk = risk;
	}
}
