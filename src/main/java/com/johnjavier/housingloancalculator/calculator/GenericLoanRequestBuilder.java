package com.johnjavier.housingloancalculator.calculator;

public class GenericLoanRequestBuilder {

	private final double initialDeposit;

	private final double monthlyIncome;

	private final double interestRate;

	private final int term;

	private final double monthlyExpenses;

	private final double principal;

	public GenericLoanRequestBuilder(Builder builder) {
		this.initialDeposit = builder.initialDeposit;
		this.monthlyIncome = builder.monthlyIncome;
		this.interestRate = builder.interestRate;
		this.term = builder.term;
		this.monthlyExpenses = builder.monthlyExpenses;
		this.principal = builder.principal;
	}

	public static class Builder {
		private double initialDeposit;

		private double monthlyIncome;

		private double interestRate;

		private int term;

		private double monthlyExpenses;

		private double principal;

		public static Builder newInstance() {
			return new Builder();
		}

		public Builder setInitialDeposit(double initialDeposit) {
			this.initialDeposit = initialDeposit;
			return this;
		}

		public Builder setMonthlyIncome(double monthlyIncome) {
			this.monthlyIncome = monthlyIncome;
			return this;
		}

		public Builder setInterestRate(double interestRate) {
			this.interestRate = interestRate;
			return this;
		}

		public Builder setTerm(int term) {
			this.term = term;
			return this;
		}

		public Builder setPrincipal(double principal) {
			this.principal = principal;
			return this;
		}

		public Builder setMonthlyExpenses(double monthlyExpenses) {
			this.monthlyExpenses = monthlyExpenses;
			return this;
		}

		public GenericLoanRequestBuilder build() {
			return new GenericLoanRequestBuilder(this);
		}

	}

	public double getInitialDeposit() {
		return initialDeposit;
	}

	public double getMonthlyIncome() {
		return monthlyIncome;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public int getTerm() {
		return term;
	}

	public double getMonthlyExpenses() {
		return monthlyExpenses;
	}

	public double getPrincipal() {
		return principal;
	}

}
