package com.johnjavier.housingloancalculator.calculator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.johnjavier.housingloancalculator.config.HousingLoanCalculatorConfiguration;

@Component
public class GenericHousingLoanCalculator {

	private GenericLoanRequestBuilder genericLoanDetails;

	@Autowired
	private HousingLoanCalculatorConfiguration housingLoanConfig;

	public void setGenericLoanRequestBuilder(GenericLoanRequestBuilder genericLoanDetails) {
		this.genericLoanDetails = genericLoanDetails;
	}

	/**
	 * Calculate maximum monthly mortgage allowed
	 * 
	 * @return
	 */
	public double calculateMaxMonthlyMortgage() {

		return (genericLoanDetails.getMonthlyIncome() - genericLoanDetails.getMonthlyExpenses())
				* (housingLoanConfig.getRisk() / 100);

	}

	/**
	 * Calculate Loanable mortgage amount base on a given interest rate, term,
	 * montnly expenses and income
	 * 
	 * @return
	 */
	public double calculateLoanableMortgage() {
		double dividend = Math.pow((1 + (convertedPercentToDecimal(genericLoanDetails.getInterestRate()) / 100)),
				-genericLoanDetails.getTerm());

		double fullDividend = calculateMaxMonthlyMortgage() * (1 - dividend);

		return fullDividend / (convertedPercentToDecimal(genericLoanDetails.getInterestRate()) / 100);
	}

	/**
	 * Calculate Amortization amount base on a given interest rate, principal, term,
	 * and initial deposit
	 * 
	 * @return
	 */
	public double calculateAmortizationAmount() {
		double princiaplLessInitialDeposit = genericLoanDetails.getPrincipal() - genericLoanDetails.getInitialDeposit();

		double dividend = convertedPercentToDecimal(genericLoanDetails.getInterestRate()) * Math
				.pow(1 + convertedPercentToDecimal(genericLoanDetails.getInterestRate()), genericLoanDetails.getTerm());

		double divisor = Math.pow(1 + convertedPercentToDecimal(genericLoanDetails.getInterestRate()),
				genericLoanDetails.getTerm()) - 1;

		return princiaplLessInitialDeposit * (dividend / divisor);

	}

	private double convertedPercentToDecimal(double interestRate) {
		return interestRate / 100;
	}

}
