package com.johnjavier.housingloancalculator.calculator;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.johnjavier.housingloancalculator.config.HousingLoanCalculatorConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
public class GenericHousingLoanCalculatorTest {

	@Mock
	private GenericLoanRequestBuilder genericLoanRequestBuilder;

	@Mock
	private HousingLoanCalculatorConfiguration housingLoanCalculatorConfig;

	@InjectMocks
	private GenericHousingLoanCalculator calculator;

	@Before
	public void setup() {
		when(genericLoanRequestBuilder.getMonthlyIncome()).thenReturn(100000.00);
		when(genericLoanRequestBuilder.getMonthlyExpenses()).thenReturn(50000.00);
		when(genericLoanRequestBuilder.getPrincipal()).thenReturn(7000000.00);
		when(genericLoanRequestBuilder.getTerm()).thenReturn(12);
		when(genericLoanRequestBuilder.getInterestRate()).thenReturn(12.0);
		when(housingLoanCalculatorConfig.getRisk()).thenReturn(36.0);
		calculator.setGenericLoanRequestBuilder(genericLoanRequestBuilder);
	}

	@Test
	public void testCalculateMaxMonthlyMortgage() {

		assertEquals(18000.00, calculator.calculateMaxMonthlyMortgage(), 0.01);
	}

	@Test
	public void testCalculateLoanableMortgage() {

		assertEquals(214324.59, calculator.calculateLoanableMortgage(), 0.01);
	}

	@Test
	public void testCalculateAmortizationAmount() {
		assertEquals(1130057.65, calculator.calculateAmortizationAmount(), 0.01);
	}

}
