package com.johnjavier.housingloancalculator.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.johnjavier.housingloancalculator.model.AmortizationRequest;
import com.johnjavier.housingloancalculator.model.AmortizationResponse;
import com.johnjavier.housingloancalculator.model.LoanRequest;
import com.johnjavier.housingloancalculator.model.LoanResponse;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class HousingLoanCalculatorServiceTest {

	@Autowired
	private HousingLoanCalculatorService housingLoanCalculatorService;

	@Test
	public void testGetMortgage() {
		LoanRequest request = new LoanRequest();
		request.setInitialDeposit(0);
		request.setInterestRate(5);
		request.setMonthlyExpenses(800);
		request.setMonthlyIncome(2000);
		request.setTerm(12);

		LoanResponse response = housingLoanCalculatorService.getMortgage(request);

		assertEquals(5167.19, response.getLoanableMortgage(), 0.01);
	}

	@Test
	public void testGetAmortizationDetails() {
		AmortizationRequest request = new AmortizationRequest();
		request.setInitialDeposit(0);
		request.setInterestRate(1);
		request.setPrincipalLoanAmount(20000);
		request.setTerms(60);

		AmortizationResponse response = housingLoanCalculatorService.getAmortizationDetails(request);

		assertEquals(444.88, response.getAmortization(), 0.01);
	}
}
