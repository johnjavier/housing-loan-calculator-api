package com.johnjavier.housingloancalculator.controller;

import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Validator;

import com.johnjavier.housingloancalculator.model.AmortizationRequest;
import com.johnjavier.housingloancalculator.model.AmortizationResponse;
import com.johnjavier.housingloancalculator.model.LoanRequest;
import com.johnjavier.housingloancalculator.model.LoanResponse;
import com.johnjavier.housingloancalculator.service.HousingLoanCalculatorService;

@RunWith(SpringJUnit4ClassRunner.class)
public class HousingLoanCalculatorControllerTest {

	private MockMvc mockMvc;

	private final String HOUSING_LOAN_CONTROLLER_PATH = "/loan";

	private final String GET_AMORTIZATION_DETAILS = "/amortization";

	@InjectMocks
	private HousingLoanCalculatorController housingLoanCalculatorController;

	@MockBean
	private HousingLoanCalculatorService housingLoanCalculatorService;

	@MockBean
	private Validator mockValidator;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(housingLoanCalculatorController).setValidator(mockValidator).build();
	}

	@Test
	public void testGetLoan() {

		LoanResponse response = new LoanResponse();
		response.setLoanableMortgage(1000);
		response.setMaxMonthlyMortgage(500);

		LoanRequest request = new LoanRequest();
		request.setInitialDeposit(10);
		request.setInterestRate(12);
		request.setMonthlyExpenses(50000);
		request.setMonthlyIncome(100000);
		request.setTerm(12);

		Mockito.when(housingLoanCalculatorService.getMortgage(request)).thenReturn(response);

		try {
			mockMvc.perform(MockMvcRequestBuilders.get(HOUSING_LOAN_CONTROLLER_PATH)
					.contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE))
					.andExpect(MockMvcResultMatchers.status().isOk());
		} catch (Exception ex) {
			fail();
		}
	}

	@Test
	public void testGetAmortizationDetails() {
		AmortizationRequest request = new AmortizationRequest();

		AmortizationResponse response = new AmortizationResponse();

		Mockito.when(housingLoanCalculatorService.getAmortizationDetails(request)).thenReturn(response);

		try {
			mockMvc.perform(
					MockMvcRequestBuilders.get(GET_AMORTIZATION_DETAILS).accept(MediaType.APPLICATION_JSON_VALUE))
					.andExpect(MockMvcResultMatchers.status().isOk());
		} catch (Exception exception) {
			fail();
		}
	}
}
