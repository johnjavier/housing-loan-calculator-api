### Mortgage Calculator

This is a spring boot application that calculates the maximum mortgage you can afford base on your salary and the initial deposit you will make

### Build Pipeline

Build and test execution can be found here: https://gitlab.com/johnjavier/housing-loan-calculator-api/pipelines

### Build Pipeline
Swagger documentation of the REST API can be found there: http://housingloancalculatorapi-env.ikxz8vzmpy.us-east-2.elasticbeanstalk.com/swagger-ui.html#/

## TASK DONE
1. Created spring boot REST API project
2. Unit Test
3. Swagger Documentation
3. Gitlab CI
4. Deploy in AWS
5. 
